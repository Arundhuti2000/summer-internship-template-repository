// Create instance of middleware
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// Create instance of mysql package
const mysql = require('mysql');
// Allow Cross origin support
var cors = require('cors');
// Configure middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
 
// connection configurations
const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'project11'
});
 
// connect to database
mc.connect();
 
// API for User Login
app.post('/checklogin', function (req, res) {
 
   // Get Form values using http request transfered from User application
    let l_name = req.body.username;
    let l_pwd = req.body.password;
   
// Run SQL query to varify username and password
    mc.query('SELECT id FROM login where username=? AND password=?',[l_name ,l_pwd] ,function (error, results, fields) {
       // if error throw error
        if (error) throw error;
        // if credentials are correct then check that default 4 API are already inserted or not, if not then run insert command to insert 4 default API
        if(results[0]!=null){
            mc.query('select id from images where username = ?', [l_name], function(error, results, fields){
                if(error) throw error;
                if(results[0]!= null){
                    return res.send({ error: false, data: results[0], message: results[0].id });
                }
                else{
                    // Insert 4 default API
                    let stmt = `INSERT INTO images(file_name,img_order,username, widget)  VALUES ?  `;
                    let todos = [
                      ['1.jpg',1,  l_name , '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-5767dcc0-3889-4e57-9521-86260036af5c"></div>'],
                      ['2.jpg',2,  l_name , '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-33af0aed-5c2d-44a5-8a6d-9778c0e0b383"></div>'],
                      ['3.jpg',3,  l_name , '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-b8532e73-1022-4fa0-b552-333aa77ac541"></div>'],
                      ['4.jpg',4,  l_name, '<script src="https://apps.elfsight.com/p/platform.js" defer></script>  <div class="elfsight-app-ff4ec089-8177-4953-a9c4-3ccc15daa7d0"></div>']
                  
                    ];
                    // execute the insert statment
                    mc.query(stmt, [todos], (err, results, fields) => {
                        if (err) {
                          return console.error(err.message);
                        }
                        // get inserted rows
                        console.log('Row inserted:' + results.affectedRows);
                        return res.send({ error: false, data: results[0], message: 'Record Inserted'});
                      });
                }
            })
           
        }
        else{
            return res.send({ error: true,  message: 'Not Get Data.', status : 201 });
        }
       
    });
 
});
// Add a new User on Registration.html
app.post('/adduser', function (req, res) {
  
    // Get User Data using http request object
    let name = req.body.name;
    let username = req.body.username;
    let password = req.body.password;
    let email = req.body.email;

  //console.log(name+" "+username +" "+password+" "+email);
  // check user supplied all values or not, if not then send http status 400
    if (!name && !username && !password && !email) {
        return res.status(400).send({ error:true, message: 'Please provide Information to be add' });
    }
    // Write insert query for registration  
    mc.query("INSERT INTO login(name, username, password, email) value(?,?,?,?) ", [name,username, password, email], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Record has been added' });
    });
});
// Retrieve MoreWidgets
app.get('/morewidgets/:username', function (req, res) {
    // Get UserName
    let username = req.params.username;
    // Get Those  user specific widgets from widget table which are not already added in images table 
    mc.query('SELECT * FROM widget where url NOT IN(SELECT widget from images WHERE username=?)',[username], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Complete Data.' });
    });
});
// Add a new Widget -- Morewidgets.html functionality API
app.post('/addwidget', function (req, res) {
  
    // Get API URL and username
    let url = req.body.url;
    let username = req.body.username;

// Check Username and URL are transfered or not
    if (!url && !username) {
        return res.status(400).send({ error:true, message: 'Please provide Information to be add' });
    }
    // Write Logic to add widgets in images table
    // Get max img_order and add 1 into it to add newly widget image order
    mc.query('SELECT id FROM images where username=? and widget=?',[username ,url], function (error, results, fields) {
        if (error) throw error;
        // if widget already added then no need to add same api widget again
        if(results[0]!=null){
            
            return res.send({ error: false, data: results[0], message: 'Already added this widget' });
        }
        else{
            // Add Widget- Get Image order and increment it with one and insert new widget in images table
            mc.query('SELECT MAX(img_order) AS MaxOrder  FROM images where username=?',[username], function(error, results, fields) {
                           
               // Increment Image Order
               let image_order = parseInt(results[0].MaxOrder)+1;
               // Insert Statement
               mc.query("INSERT INTO images(file_name, img_order, username, widget) values(?,?,?,?) ", ['new.jpg', image_order ,username, url], function (error, results, fields) {
                if (error) throw error;
                return res.send({ error: false, data: results, message: 'Record has been added' });
            });
            })
        }
        
    });
  
});

 app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});