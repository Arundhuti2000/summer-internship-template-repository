<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="style.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
    //var unamevalue = localStorage.getItem("username");
    //$("#uname").val(unamevalue);
    $('.reorder_link').on('click',function(){
        $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
        $('.reorder_link').html('save reordering');
        $('.reorder_link').attr("id","saveReorder");
        $('#reorderHelper').slideDown('slow');
        $('.image_link').attr("href","javascript:void(0);");
        $('.image_link').css("cursor","move");
        
        $("#saveReorder").click(function( e ){
            if( !$("#saveReorder i").length ){
                $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                $("ul.reorder-photos-list").sortable('destroy');
                //$("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');
                
                var h = [];
                $("ul.reorder-photos-list li").each(function() {
                    h.push($(this).attr('id').substr(9));
                });
                var usernamevalue = $("uname_hidden").val();
                $.ajax({
                    type: "POST",
                    url: "orderUpdate.php",
                    data: {ids: " " + h + "", username : usernamevalue },
                    success: function(){
                        //window.location.reload();
                        sessionStorage.removeItem('uname');
                        location.href='login.html';

                    }
                });	
                return false;
            }	
            e.preventDefault();
        });
    });
});
</script>
</head>
<body>
<?php 
function getRows($username2){ 
    $servername = "localhost";
$username = "root";
$password = "";
$dbname = "nilabh";

// Create connection
$db = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($db->connect_error) {
    die("Connection failed: " . $db->connect_error);
}
else{
   // echo 'OK';
}

    //echo 'username is '.$username;
   //echo $username2;
    //$this->uname.set()
    //echo $this->uname;
    $sql = "select * from images where username = '".$username2."' ORDER BY img_order ASC";
   // echo $sql;
    $result1 = $db->query($sql); 
    if (!$result1) {
        trigger_error('Invalid query: ' . $db->error);
    }
    //$query = $this->db->query("SELECT * FROM ".$this->imgTbl." WHERE username = '".$this->uname1."' ORDER BY img_order ASC"); 
    //echo $query->num_rows;
    //echo $query;
    if($result1->num_rows > 0){ 
        while($row = $result1->fetch_assoc()){ 
            $result[] = $row; 
        } 
    }else{ 
        $result = FALSE; 
    } 
    return $result; 
} 


?>
   
<div class="container">
    <a href="javascript:void(0);" class="reorder_link" id="saveReorder">reorder photos</a>
    <div id="reorderHelper" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
    <div class="gallery">
        <ul class="reorder_ul reorder-photos-list">
       
            
        <?php 
        // Include and create instance of DB class 
        //setcookie("cookie1", $uname, time()+2*24*60*60);
        require_once 'DB.class.php'; 
        $db = new DB(); 
       // $uname = ; 
        //echo 'uname value is '.$uname;
        // Fetch all images from database 
       // $fetchusername = '<script>document.write(sessionStorage.getItem("uname"))</script>';
        $fetchusername = $_COOKIE['uname'];
        $user =  $fetchusername;
     // echo 'Cookie value is '.$_COOKIE['uname'];
        $images = getRows($_COOKIE['uname']); 
        if(!empty($images)){ 
            foreach($images as $row){ 
        ?>
            <li id="image_li_<?php echo $row['id']; ?>" class="ui-sortable-handle">
                <a href="javascript:void(0);" style="float:none;" class="image_link">
                    <img src="images/<?php echo $row['file_name']; ?>" alt="">
                </a>
            </li>
        <?php } } ?>
        </ul>
        <input type="hidden" id="uname_hidden" name="uname_hidden" value="<?php echo $_COOKIE['uname'] ?>" />
    </div>
</div>
</body>
</html>