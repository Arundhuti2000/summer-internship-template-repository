-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2019 at 01:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nilabh`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `file_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img_order` int(5) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `widget` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `file_name`, `img_order`, `created`, `modified`, `username`, `widget`) VALUES
(1, '1.jpg', 2, '0000-00-00 00:00:00', '2019-10-08 16:25:40', 'user1', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-5767dcc0-3889-4e57-9521-86260036af5c"></div>'),
(2, '2.jpg', 4, '0000-00-00 00:00:00', '2019-10-08 16:25:40', 'user1', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-33af0aed-5c2d-44a5-8a6d-9778c0e0b383"></div>'),
(3, '3.jpg', 1, '0000-00-00 00:00:00', '2019-10-08 16:25:40', 'user1', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-b8532e73-1022-4fa0-b552-333aa77ac541"></div>'),
(4, '4.jpg', 3, '0000-00-00 00:00:00', '2019-10-08 16:25:40', 'user1', '<script src="https://apps.elfsight.com/p/platform.js" defer></script>\n<div class="elfsight-app-ff4ec089-8177-4953-a9c4-3ccc15daa7d0"></div>'),
(9, '1.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(10, '2.jpg', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(11, '3.jpg', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(12, '4.jpg', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(13, '5.jpg', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(14, '6.jpg', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(15, '7.jpg', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(16, '8.jpg', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user2', ''),
(17, '1.jpg', 2, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(18, '2.jpg', 8, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(19, '3.jpg', 6, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(20, '4.jpg', 7, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(21, '5.jpg', 5, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(22, '6.jpg', 3, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(23, '7.jpg', 1, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(24, '8.jpg', 4, '0000-00-00 00:00:00', '2019-10-08 08:19:00', 'user3', ''),
(25, '1.jpg', 7, '0000-00-00 00:00:00', '2019-10-08 08:47:17', 'user4', ''),
(26, '2.jpg', 8, '0000-00-00 00:00:00', '2019-10-08 08:47:17', 'user4', ''),
(27, '3.jpg', 2, '0000-00-00 00:00:00', '2019-10-08 08:47:16', 'user4', ''),
(28, '4.jpg', 3, '0000-00-00 00:00:00', '2019-10-08 08:47:16', 'user4', ''),
(29, '5.jpg', 5, '0000-00-00 00:00:00', '2019-10-08 08:47:17', 'user4', ''),
(30, '6.jpg', 6, '0000-00-00 00:00:00', '2019-10-08 08:47:17', 'user4', ''),
(31, '7.jpg', 1, '0000-00-00 00:00:00', '2019-10-08 08:47:16', 'user4', ''),
(32, '8.jpg', 4, '0000-00-00 00:00:00', '2019-10-08 08:47:16', 'user4', ''),
(33, '1.jpg', 2, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(34, '2.jpg', 7, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(35, '3.jpg', 4, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(36, '4.jpg', 5, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(37, '5.jpg', 8, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(38, '6.jpg', 1, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(39, '7.jpg', 6, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(40, '8.jpg', 3, '0000-00-00 00:00:00', '2019-10-08 14:19:27', 'user5', ''),
(41, '4.jpg', 5, '0000-00-00 00:00:00', '2019-10-08 16:25:40', 'user1', '<script src="https://apps.elfsight.com/p/platform.js" defer></script>\r\n<div class="elfsight-app-ff4ec089-8177-4953-a9c4-3ccc15daa7d0"></div>'),
(42, '1.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user6', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-5767dcc0-3889-4e57-9521-86260036af5c"></div>'),
(43, '2.jpg', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user6', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-33af0aed-5c2d-44a5-8a6d-9778c0e0b383"></div>'),
(44, '3.jpg', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user6', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-b8532e73-1022-4fa0-b552-333aa77ac541"></div>'),
(45, '4.jpg', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'user6', '<script src="https://apps.elfsight.com/p/platform.js" defer></script>  <div class="elfsight-app-ff4ec089-8177-4953-a9c4-3ccc15daa7d0"></div>'),
(46, '1.jpg', 3, '0000-00-00 00:00:00', '2019-10-08 16:54:09', 'user7', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-5767dcc0-3889-4e57-9521-86260036af5c"></div>'),
(47, '2.jpg', 4, '0000-00-00 00:00:00', '2019-10-08 16:54:09', 'user7', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-33af0aed-5c2d-44a5-8a6d-9778c0e0b383"></div>'),
(48, '3.jpg', 1, '0000-00-00 00:00:00', '2019-10-08 16:54:09', 'user7', '<script src="https://apps.elfsight.com/p/platform.js" defer></script> <div class="elfsight-app-b8532e73-1022-4fa0-b552-333aa77ac541"></div>'),
(49, '4.jpg', 2, '0000-00-00 00:00:00', '2019-10-08 16:54:09', 'user7', '<script src="https://apps.elfsight.com/p/platform.js" defer></script>  <div class="elfsight-app-ff4ec089-8177-4953-a9c4-3ccc15daa7d0"></div>');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'user1', 'user1'),
(2, 'user2', 'user2'),
(3, 'user3', 'user3'),
(4, 'user4', 'user4'),
(5, 'user5', 'user5'),
(6, 'user6', 'user6'),
(7, 'user7', 'user7'),
(8, 'user8', 'user8');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
